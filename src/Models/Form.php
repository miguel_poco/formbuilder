<?php namespace float\formBuilder\Models;

use Illuminate\Support\Facades\View;

use float\formBuilder\Models\Field;

use App\Models\Contents\Content_model;

/**
 * Dear developer: 
 * Once you are done trying to 'optimize' this routine, 
 * and have realized what a terrible mistake that was, 
 * please increment the following counter as a warning
 * to the next guy: 
 * 
 * total_hours_wasted_here = 40
 */

class Form{
	
	public $model_obj, $model, $custom, $edit, $buttons, $hasDates, $hasFiles;
	public $options = [];
	public $fields = [];
	public $submodels = [];
	
	public static $default_options = [
		'id' => 'default',
		'class' => 'fupload form-horizontal',
	];
	
	private static $default_view_path = 'forms.default.';
	
	public function __construct($model_obj, $custom, $custom_view = null){
		
		$this->model_obj = $model_obj;
		
		$reflect = new \ReflectionClass($model_obj);
		$this->model = strtolower($reflect->getShortName());
		
		$this->custom = $custom;
		
		$this->view_path = !empty($custom_view) ? 'forms.' . $custom_view . '.' : static::$default_view_path;
		
		$this->edit = isset($this->model_obj->id) ? $this->model_obj->id : false;
		
		$this->treatForm();
		
		$this->treatFields();
		
		$this->treatSubmodels();
		
		$this->treatButtons();
		
		$this->treatScripts();
		
		$this->options = isset($this->custom['options']) ? array_merge(static::$options, $this->custom['options']) : static::$default_options;
		
	}
	
	public function treatForm(){}
	
	public function treatFields(){
		//Set excluded fields
		$this->fields['exclude'] = isset($this->custom['fields']['exclude']) ? $this->custom['fields']['exclude'] : [];
		//Merge with default excludes
		$this->fields['exclude'] = $this->edit ?
			array_merge($this->fields['exclude'], ['id', 'deleted_at']) : 
			array_merge($this->fields['exclude'], ['id', 'created_at', 'updated_at', 'deleted_at']);
		
		
		foreach($this->model_obj->getAllFields() as $name=>$field){
			//Treat field options
			$options = $this->treatFieldOptions($name, $field);
			
			//Instantiate fields
			if(!in_array($name, $this->fields['exclude']))
				$this->fields['elements'][] = new Field($options);
		}
		
		//Add "new__" field for identifying new field
		if(!$this->edit){
			$options = $this->treatFieldOptions("new__", [
				"table_type" => "hidden",
				"value"	=> 1
			]);
			$this->fields['elements'][] = new Field($options);
		}
	}
	
	public function treatFieldOptions($name, $field){
		
		return [
			'name' => $this->treatName($name),
			'placeholder' => $name,
			'type' => $field['table_type'],
			'value' => isset($field['value']) ? $field['value'] :$this->model_obj->$name,
			'view_path' => $this->view_path
		];
	}
	
	public function treatName($name){
		return isset($this->custom['fields']['name_prefix']) ? 
			$this->custom['fields']['name_prefix']."[$name]" : $name;
	}
	
	public function treatButtons(){
		//Set excluded buttons
		$this->buttons['exclude'] = isset($this->custom['buttons']['exclude']) ?
					$this->custom['buttons']['exclude'] : [];
		
		$view = View::exists($this->view_path.'.buttons') ? 
					$this->view_path.'.buttons' : 'forms.default.buttons';
		
		$this->buttons['html'] = view($view, [
			'buttons' => $this->buttons,
			'edit' => $this->edit
		]);
			
	}
	
	
	public function build(){
		
		$layout = View::exists($this->view_path.'.layout') ? 
					$this->view_path.'.layout' : 'forms.default.layout';
		
		$html = view($layout, ['form' => $this]);
		
		return $html;
	}
	
	public function treatScripts(){
		
		foreach($this->fields['elements'] as $field){
			
			if($field->type == 'file' && !empty($field->value) )
				$this->hasFiles = true;
			if($field->type == 'date' && !$field->disabled)
				$this->hasDates = true;
		}
		
	}
	
	//instantiates empty submodels for formbuilding
	public function treatSubmodels(){
		$ctmodel = Content_model::where('class_name', $this->model)->first();
		
		if(!empty($ctmodel)){
			$submodels = $ctmodel->submodels()->get();

			foreach($submodels as $submodel){
				$this->submodels[$submodel->pivot->name] = $submodel;
				if($this->edit){
					$this->submodels[$submodel->pivot->name]['items'] = $submodel->getItems($this->model_obj->id);
				}
			}
		}
	}
	
}