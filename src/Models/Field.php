<?php namespace float\formBuilder\Models;

use Illuminate\Support\Facades\View;

/** 
* For the brave souls who get this far: You are the chosen ones, 
* the valiant knights of programming who toil away, without rest, 
* fixing our most awful code. To you, true saviors, kings of men, 
* I say this: never gonna give you up, never gonna let you down, 
* never gonna run around and desert you. Never gonna make you cry,
* never gonna say goodbye. Never gonna tell a lie and hurt you. 
*/

class Field{

	public $name, $placeholder, $type, $value, $view_path, $container, $template, $disabled, $css_class;
	
	public static $types = [
		'var' => 'text',
		'tex' => 'textarea',
		'dat' => 'date',
		'tim' => 'date',
		'int' => 'number',
		'file' => 'file',
		'hidden' => 'hidden',
		'tin' => 'number'
	];
	
	public function __construct($options){
		
		$this->name = $options['name'];
		$this->placeholder = $options['placeholder'];
		
		
		$this->type = isset(Static::$types[$options['type']]) ? Static::$types[$options['type']] : $options['type'];
		$this->value = $options['value'];
		$this->view_path = $options['view_path'];
		$this->disabled = in_array($this->name, ['created_at', 'updated_at', 'deleted_at']);
		
		$this->css_class = $this->treatCss();
		$this->container = $this->getContainer();
		$this->template = $this->getTemplate();
		
	}
	
	public function treatCss(){
		if($this->type == 'date' && !$this->disabled)
			$this->css_class = 'isDate';
	}
	
	public function getContainer(){
		if(View::exists($this->view_path.'fields.container'))
			return $this->view_path.'fields.container';
		else
			return 'forms.default.fields.container';
	}
	
	public function getTemplate(){
		if(View::exists($this->view_path.'fields.'.$this->name))
			return $this->view_path.'.fields.'.$this->name;
		elseif(View::exists($this->view_path.'.fields.'.$this->type))
			return $this->view_path.'.fields.'.$this->type;
		else
			return 'forms.default.fields.'.$this->type;
	}
}