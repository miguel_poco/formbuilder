<?php

namespace float\formBuilder;

use Illuminate\Support\ServiceProvider;
use float\formBuilder\Models\FormBuilder;
use float\formBuilder\Models\HtmlBuilder;

class formBuilderServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(){
        //Lets publish files to the respective folders
		$this->publishes([
			__DIR__.'/Traits' => app_path('Models/Traits'),
			__DIR__.'/views' => resource_path('views/forms/default')
		]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(){
        //Provide alias for view facades
		$this->registerHtmlBuilder();
		$this->app->alias('html', 'float\formBuilder\Models\HtmlBuilder');
		
		$this->registerFormBuilder();
		$this->app->alias('form', 'float\formBuilder\Models\\FormBuilder');
    }
	
	/**
	 * Register the HTML builder instance.
	 *
	 * @return void
	 */
	protected function registerHtmlBuilder()
	{
		$this->app->singleton('html', function($app)
		{
			return new HtmlBuilder($app['url']);
		});
	}
	
	/**
	 * Register the form builder instance.
	 *
	 * @return void
	 */
	protected function registerFormBuilder()
	{
		$this->app->singleton('form', function($app)
		{
			$form = new FormBuilder($app['html'], $app['url'], $app['session.store']->getToken());

			return $form->setSessionStore($app['session.store']);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('html', 'form', 'float\formBuilder\Models\HtmlBuilder', 'float\formBuilder\Models\FormBuilder');
	}
}