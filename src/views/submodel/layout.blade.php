<div class="card col-xs-3">
	<img class="card-img-top" src="..." alt="Card image cap">
	<div class="card-block">
	@foreach($form->fields['elements'] as $field)

		@include($field->template, ['field' => $field])

	@endforeach
	</div>
</div>