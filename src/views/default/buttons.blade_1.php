<!-- Buttons outer container -->
<div class="col-md-12">
	<div class="form-group">
		
		@ifnot(in_array('submit', $buttons['exclude']))
		<button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check"></i> 
			{{ $edit ? Lang::get('backoffice/main.edit') : Lang::get('backoffice/main.create')}}
		</button>
		@endif

		@ifnot(in_array('reset', $buttons['exclude']))
		<button class="btn btn-sm btn-default" type="reset"><i class="fa fa-undo"></i> 
			{{Lang::get('backoffice/main.reset')}}
		</button>
		@endif

		@ifnot(in_array('cancel', $buttons['exclude']))
		<button class="btn btn-sm btn-warning close_popup" type="reset"><i class="fa fa-ban"></i>
			{{Lang::get('backoffice/main.cancel')}}
		</button>
		@endif

	</div>
</div>