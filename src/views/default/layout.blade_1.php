{!! Form::open(['class' => 'fupload form-horizontal']) !!}

@foreach($form->fields['elements'] as $field)
	
	@include($field->template, ['field' => $field])

@endforeach

<div class="clearfix"></div>

@if(!empty($form->submodels))
	@foreach($form->submodels as $name => $submodel)
	<div class="submodel_container">
		<h2 class="text-xs-center">{{$name}}</h2>
		@form($submodel, [], "submodel")
	</div>
	@endforeach
@endif

{!! $form->buttons['html'] !!}

{!! Form::close() !!}

@section('scripts')
	@include('forms.default.scripts.filer')
	
	@if($form->hasFiles)
		@include('forms.default.scripts.deleteFile')
		
	@endif

	@if($form->hasDates)
		@include('forms.default.scripts.date')
	@endif

@stop