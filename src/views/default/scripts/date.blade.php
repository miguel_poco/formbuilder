<script type="text/javascript">
$(".isDate").datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: "yy-mm-dd",
	yearRange: "-100:+0",
	dayNamesMin: ["Do", "Se", "Te", "Qa", "Qi", "Se", "Sa"],
	monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
});
</script>