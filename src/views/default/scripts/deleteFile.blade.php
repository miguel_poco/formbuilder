<script type="text/javascript">
$(".deleteField").click(function(){
	id_file = $(this).attr('data-id');
	field = $(this).attr('data-field');
	parent = $(this).parents(".form-group2");
	$.confirm({
		title: "Apagar ficheiro",
		content: "Tem a certeza que deseja eliminar?",
		confirmButtonClass: "btn-danger",
		confirm: function(button){
			$.ajax({
				type: "POST",
				url: "{{URL::to('backoffice/contents/'.$model.'/deleteField')}}",
				data: {
					"id": id_file,
					"field": field
				}
			}).done(function(data){
				console.log(data);
				if(data == true){
					parent.remove();
				}
			});
		},
		confirmButton: "Sim",
		cancelButton: "Não"
	});	
});
</script>