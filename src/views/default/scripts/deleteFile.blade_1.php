<script type="text/javascript">
$(".deleteField").click(function(){
	id_file = $(this).attr('data-id');
	field = $(this).attr('data-field');
	parent_container = $(this).parents(".form-group2");
	$.confirm({
		title: "Apagar ficheiro",
		content: "Tem a certeza que deseja eliminar?",
		confirmButtonClass: "btn-danger",
		confirm: function(button){
			$.ajax({
				type: "POST",
				url: "{{URL::to('backoffice/contents/'.$form->model.'/delete-file')}}",
				data: {
					"id": id_file,
					"field": field,
					"_token": "{{csrf_token()}}"
				}
			}).done(function(data){
				if(data == true){
					parent_container.remove();
				}
			});
		},
		confirmButton: "Sim",
		cancelButton: "Não"
	});	
});
</script>