@extends($field->container)
@section('label')
<label class="col-md-2 control-label" for="{{$field->name}}">
	{{Lang::has('backoffice/main.'.$field->name) ? Lang::get('backoffice/main.'.$field->name) : ucfirst($field->name)}}
</label>
@overwrite @section('field')
<input class="form-control isDate" type="text" name="{{$field->name}}" placeholder="{{Lang::get('backoffice/main.'.$field->name)}}" id="{{$field->name}}" value="{{$field->value or ''}}" {{$field->disabled ? 'disabled="disabled"' : ''}}>
@overwrite @section('auxiliary')
@overwrite