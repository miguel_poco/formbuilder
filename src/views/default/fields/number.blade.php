@extends($field->container)
@section('label')
<label class="col-md-2 control-label" for="{{$field->name}}">
	{{Lang::has('backoffice/main.'.$field->name) ? Lang::get('backoffice/main.'.$field->name) : ucfirst($field->name)}}
</label>
@overwrite @section('field')
<input class="form-control" type="number" name="{{$field->name}}" 
	   placeholder="{{Lang::has('backoffice/main.'.$field->name) ? Lang::get('backoffice/main.'.$field->name) : ucfirst($field->name)}}" id="{{$field->name}}" value="{{$field->value or ''}}" {{$field->disabled ? 'disabled="disabled"' : ''}}>
@overwrite @section('auxiliary')
@overwrite