<div class="col-md-12">
	<div class="form-group">
		@yield('label')
		<div class="col-md-9">
			@yield('field')
		</div>
	</div>
	<div class="form-group2 col-md-9 col-md-offset-2">
		@yield('auxiliary')
	</div>
</div>