@extends($field->container)
@section('label')
<label class="col-md-2 control-label" for="{{$field->name}}">
	{{Lang::has('backoffice/main.'.$field->name) ? Lang::get('backoffice/main.'.$field->name) : ucfirst($field->name)}}
</label>
@overwrite @section('field')
<textarea class="form-control" name="{{$field->name}}" id="{{$field->name}}">
	{{$field->value or ''}}
</textarea>
@overwrite @section('auxiliary')
@overwrite