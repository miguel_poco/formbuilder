@extends($field->container)
@section('label')
<label class="col-md-2 control-label" for="{{$field->name}}">
	{{Lang::has('backoffice/main.'.$field->name) ? Lang::get('backoffice/main.'.$field->name) : ucfirst($field->name)}}
</label>
@overwrite @section('field')
<input class="form-control" type="file" name="{{$field->name}}" id="{{$field->name}}">
@overwrite @section('auxiliary')
	@if(!empty($field->value))
	<button type="button" class="btn btn-sm btn-danger deleteField" data-field="{{$field->name}}" data-id="{{$form->model_obj->id}}"> {{Lang::get('backoffice/main.delete')}}</button> Ficheiro: {{$field->value or ''}}
	@endif
@overwrite