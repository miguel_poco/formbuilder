{!! Form::open(['class' => 'fupload form-horizontal']) !!}

@foreach($form->fields['elements'] as $field)
	
	@include($field->template, ['field' => $field])

@endforeach

{!! $form->buttons['html'] !!}

{!! Form::close() !!}

@section('scripts')

	@if($form->hasFiles)
		@include('forms.default.scripts.deleteFile')
	@endif

	@if($form->hasDates)
		@include('forms.default.scripts.date')
	@endif

@stop